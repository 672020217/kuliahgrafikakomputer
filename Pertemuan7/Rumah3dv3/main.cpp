/************************************
**                                 **
** Program by Muhammad Bagus Zulmi **
**  https://mbaguszulmi.github.io  **
**                                 **
************************************/


#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include "Camera.h"
#include <stdlib.h>
#include <stdio.h>

// 1209 680
// Global Variables
GLdouble windowWidth = 853.3334,
         windowHeight = 480,
         angleX = 0,
         angleY = 0,
         scale = 1;

bool cameraMode = false;

Camera camera(0, 150, 10, 2, 3);

static void resize(int width, int height)
{
    windowWidth = width; windowHeight = height;
    camera.setMaxXY(windowWidth/2, windowHeight/2);
    const float ar = (float) width / (float) height;

    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    // glFrustum(-ar, ar, -1.0, 1.0, 1.0, 300.0);
    gluPerspective(45, ar, 1, 300);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

static void quads(
    GLfloat w, GLfloat h,
    GLfloat x, GLfloat y, GLfloat z, 
    GLfloat angle, GLfloat rx, GLfloat ry, GLfloat rz) {

    glPushMatrix();
        glTranslatef(x, y, z);
        glRotatef(angle, rx, ry, rz);
        glBegin(GL_QUADS);
            glVertex3f(w/2, h/2, 0);
            glVertex3f(w/2, -h/2, 0);
            glVertex3f(-w/2, -h/2, 0);
            glVertex3f(-w/2, h/2, 0);
        glEnd();
    glPopMatrix();
}

static void alas() {
    glColor3f(0.4745, 0.3804, 0.2392);
    // glBegin(GL_QUADS);
    //     glVertex3d(150, 0, 150);
    //     glVertex3d(-150, 0, 150);
    //     glVertex3d(-150, 0, -150);
    //     glVertex3d(150, 0, -150);
    // glEnd();

    quads(300, 300, 0, 0, 0, 90, 1, 0, 0);
}

static void jendela(int x, int y, int z) {
    glColor3f(0.6588, 0.5098, 0.102);

    glBegin(GL_QUADS);
        // atas
        glVertex3f(8+x, 8+y, z);
        glVertex3f(6+x, 6+y, z);
        glVertex3f(-6+x, 6+y, z);
        glVertex3f(-8+x, 8+y, z);
    
        // kanan
        glVertex3f(8+x, 8+y, z);
        glVertex3f(6+x, 6+y, z);
        glVertex3f(6+x, -6+y, z);
        glVertex3f(8+x, -8+y, z);

        // bawah
        glVertex3f(8+x, -8+y, z);
        glVertex3f(6+x, -6+y, z);
        glVertex3f(-6+x, -6+y, z);
        glVertex3f(-8+x, -8+y, z);

        // kiri
        glVertex3f(-8+x, -8+y, z);
        glVertex3f(-6+x, -6+y, z);
        glVertex3f(-6+x, 6+y, z);
        glVertex3f(-8+x, 8+y, z);
    glEnd();

    // kaca
    glColor4f(0, 0, 0, 0.4);
    quads(12, 12, x, y, z, 0, 0, 0, 0);
}

static void house() {
    /// dinding depan
    glColor3f(0.4824, 0.7098, 0.4902);
    quads(19, 50, -40.5, 25, 0, 0, 0, 0, 0);
    quads(8, 50, -11, 25, 0, 0, 0, 0, 0);
    quads(16, 25, -23, 37.5, 0, 0, 0, 0, 0);
    quads(16, 9, -23, 4.5, 0, 0, 0, 0, 0);

    quads(14, 25, 0, 37.5, 0, 0, 0, 0, 0);

    quads(19, 50, 40.5, 25, 0, 0, 0, 0, 0);
    quads(8, 50, 11, 25, 0, 0, 0, 0, 0);
    quads(16, 25, 23, 37.5, 0, 0, 0, 0, 0);
    quads(16, 9, 23, 4.5, 0, 0, 0, 0, 0);

    glColor3f(0.3922, 0.5569, 0.3961);
    /// dinding samping kanan
    quads(50, 50, 50, 25, -25, 90, 0, 1, 0);

    /// dinding samping kiri
    quads(50, 50, -50, 25, -25, 90, 0, 1, 0);

    glBegin(GL_TRIANGLES);
        /// atap kiri
        glVertex3f(-50, 50, 0);
        glVertex3f(-50, 50, -50);
        glVertex3f(-50, 75, -25);

        /// atap kanan
        glVertex3f(50, 50, 0);
        glVertex3f(50, 50, -50);
        glVertex3f(50, 75, -25);
    glEnd();

    /// dinding belakang
    glColor3f(0.3098, 0.4392, 0.3137);
    quads(100, 50, 0, 25, -50, 0, 0, 0, 0);

    /// atap
    glPushMatrix();
        glTranslatef(0, 75, -25);
        // belakang
        glColor3f(0.6039, 0.2745, 0.0863);
        glRotatef(45, 1, 0, 0);
        quads(110, 40, 0, -20, 0, 0, 0, 0, 0);
        // depan
        glColor3f(0.749, 0.3333, 0.0941);
        glRotatef(90, -1, 0, 0);
        quads(110, 40, 0, -20, 0, 0, 0, 0, 0);
    glPopMatrix();

    /// dinding samping depan kiri
    glColor3f(0.15, 0.15, 0.15);
    quads(50, 30, -50, 15, 25, 90, 0, 1, 0);
    glColor3f(0.2, 0.2, 0.2);
    quads(50, 30, -45, 15, 25, 90, 0, 1, 0);
    glColor3f(0.3, 0.3, 0.3);
    quads(5, 50, -47.5, 30, 25, 90, 1, 0, 0);
    glColor3f(0.4, 0.4, 0.4);
    quads(5, 30, -47.5, 15, 50, 0, 0, 0, 0);

    /// dinding samping depan kanan
    glColor3f(0.15, 0.15, 0.15);
    quads(50, 30, 50, 15, 25, 90, 0, 1, 0);
    glColor3f(0.2, 0.2, 0.2);
    quads(50, 30, 45, 15, 25, 90, 0, 1, 0);
    glColor3f(0.3, 0.3, 0.3);
    quads(5, 50, 47.5, 30, 25, 90, 1, 0, 0);
    glColor3f(0.4, 0.4, 0.4);
    quads(5, 30, 47.5, 15, 50, 0, 0, 0, 0);

    /// pintu
    glColor3f(0.6588, 0.5098, 0.102);
    quads(14, 25, 0, 12.5, 0, 0, 0, 0, 0);

    /// gagang pintu
    glColor3f(0.1, 0.1, 0.1);
    glPushMatrix();
        glTranslatef(6, 12, 0);
        glutSolidSphere(0.5, 10, 10);
    glPopMatrix();

    /// jendela kiri
    jendela(-23, 17, 0);

    /// jendela kanan
    jendela(23, 17, 0);
}

static void snowManHand(GLUquadric * quadric, int x, int y, int z, int dir) {
    GLfloat w;
    
    glColor3f(0.5588, 0.4098, 0.2);
    glPushMatrix();
        glTranslatef(x, 18+y, z);
        glRotatef(60, 0.3, dir, 0);
        glTranslatef(0, 0, 3);
        gluCylinder(quadric, 0.5, 0.4, 8, 10, 10);

        glPushMatrix();
            glTranslatef(0, 0, 8);

            for(GLfloat i = -30; i <= 30; i+=30)
            {
                glPushMatrix();
                    glRotatef(i, 1, 0, 0);
                    w = (i==0)? 0.4 : 0.2;
                    glutSolidCone(w, 2, 10, 10);
                glPopMatrix();
            }
        glPopMatrix();
    glPopMatrix();
}

static void snowMan(int x, int y, int z) {
    GLUquadricObj *quadric;
    quadric = gluNewQuadric();

    // body & head
    glColor3f(1, 1, 1);
    glPushMatrix();
        glTranslatef(x, 7+y, z);
        glutWireSphere(7, 100, 100);
    glPopMatrix();

    glPushMatrix();
        glTranslatef(x, 17+y, z);
        glutWireSphere(5, 100, 100);
    glPopMatrix();

    glPushMatrix();
        glTranslatef(x, 24+y, z);
        glutWireSphere(3, 100, 100);
    glPopMatrix();

    // left hand
    snowManHand(quadric, x, y, z, -1);

    // right hand
    snowManHand(quadric, x, y, z, 1);

    // hat
    glColor3f(0.8, 0, 0);
    glPushMatrix();
        glTranslatef(x, 26+y, z);
        glRotatef(90, -1, 0, 0);
        glutSolidCone(2, 5, 10, 10);
    glPopMatrix();

    // right eye
    glColor3f(0, 0, 0);
    glPushMatrix();
        glTranslatef(x, 24+y, z);
        glRotatef(30, -0.3, 1, 0);
        glTranslatef(0, 0, 3);
        glutSolidSphere(0.5, 10, 10);
    glPopMatrix();

    // left eye
    glPushMatrix();
        glTranslatef(x, 24+y, z);
        glRotatef(30, -0.3, -1, 0);
        glTranslatef(0, 0, 3);
        glutSolidSphere(0.5, 10, 10);
    glPopMatrix();

    // nose
    glColor3f(1, 0.549, 0);
    glPushMatrix();
        glTranslatef(x, 24+y, z+3);
        glutSolidCone(0.6, 2, 10, 10);
    glPopMatrix();
}

static void display(void)
{
    int zpos;
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glLoadIdentity();
    if (cameraMode) {
        camera.setCamera();
        zpos = 0;
    }
    else {
        gluLookAt(0, 0, 0, 0, 0, -10, 0, 1, 0);
        zpos = -150;
    }

    glPushMatrix();
        glTranslatef(0, -20, zpos);
        if (!cameraMode) {
            glRotatef(angleX, 1, 0, 0);
            glRotatef(angleY, 0, 1, 0);
            glScalef(scale, scale, scale);
        }

        alas();
        snowMan(30, 0, 20);
        house();
    glPopMatrix();

    glutSwapBuffers();
}


static void key(unsigned char key, int x, int y) {
    if (key == 'c') cameraMode = !cameraMode;

    if (cameraMode) camera.key(key);
    else {
        switch (key)
        {
            case 'q':
                exit(0);
                break;
            case 'w':
                if (scale < 3) scale += 0.1;
                printf("Scale up %f\n", scale);
                break;
            case 's':
                if (scale > 0.2) scale -= 0.1;
                printf("Scale down %f\n", scale);
                break;
            case 'r':
                angleX = 0;
                angleY = 0;
                scale = 1;
                printf("Reset view\n");
                break;
        }
    }
}

static void keySpecial(int key, int x, int y) {
    if (!cameraMode) {
        switch (key) {
            case GLUT_KEY_DOWN:
                if (angleX > -7) angleX--;
                printf("Rotate X Axis %f\n", angleX);
                break;
            case GLUT_KEY_UP:
                if (angleX < 90) angleX++;
                printf("Rotate X Axis %f\n", angleX);
                break;
            case GLUT_KEY_LEFT:
                angleY++;
                printf("Rotate Y Axis %f\n", angleY);
                break;
            case GLUT_KEY_RIGHT:
                angleY--;
                printf("Rotate Y Axis %f\n", angleY);
                break;
        }

        if (angleX == 360 || angleX == -360) angleX = 0;
        if (angleY == 360 || angleY == -360) angleY = 0;
    }
}

static void idle(void)
{
    glutPostRedisplay();
}

static void passiveMotion(int x, int y) {
    x-=windowWidth/2;
    y=(windowHeight/2)-y;
    camera.mouseMove(x, y);
    printf("Mouse x %d, y %d\n", x, y);
}

static void init() {
    camera.setMaxXY(windowWidth/2, windowHeight/2);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC1_ALPHA);
    glEnable(GL_BLEND);
    glClearColor(0.1725, 0.1725, 0.251, 1);
    glEnable(GL_DEPTH_TEST);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45, windowWidth/windowHeight, 1, 300);
    // glFrustum(-ar, ar, -1.0, 1.0, 1.0, 300.0);
    glMatrixMode(GL_MODELVIEW);
}

int main(int argc, char *argv[])
{
    glutInit(&argc, argv);
    glutInitWindowSize(windowWidth, windowHeight);
    glutInitWindowPosition(300, 150);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);

    glutCreateWindow("Rumah 3D");

    glutReshapeFunc(resize);
    glutDisplayFunc(display);
    glutKeyboardFunc(key);
    glutSpecialFunc(keySpecial);
    glutIdleFunc(idle);
    glutPassiveMotionFunc(passiveMotion);

    init();
    glutMainLoop();

    return EXIT_SUCCESS;
}
