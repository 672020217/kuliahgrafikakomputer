#include <GL/glut.h>
#include "glm.h"

GLfloat angle = 0;

void resize(int w, int h) {
    const float ar = (float) w / (float) h;

    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45, ar, 1, 100);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

GLMmodel *deer = NULL;

void drawModel() {
    if (!deer) {
        deer = glmReadOBJ("deer.obj");
        if (!deer) exit(0);
        glmUnitize(deer);
        glmFacetNormals(deer);
        glmVertexNormals(deer, 90.0);
    }
    glmDraw(deer, GLM_SMOOTH);
}

void display() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_TEXTURE_2D);
    glPushMatrix();
        glTranslatef(0, 0, -5);
        glRotatef(angle++, 0, 1, 0);
        // glColor3f(1, 0, 0);
        drawModel();
    glPopMatrix();

    glDisable(GL_TEXTURE);
    glutSwapBuffers();
}

void idle() {
    glutPostRedisplay();
}



void init() {
    GLfloat lightPos[] = {0, 0, 0.1, 20, 0};
    GLfloat lightAmbient[] = {0, 0, 0, 0};
    GLfloat lightDiffuse[] = {0.7, 0.7, 0.7, 0.1};
    GLfloat lightSpecular[] = {0.5, 0.5, 0.5, 0.1};
    GLfloat shine[] = {80};

    glClearColor(0, 0, 0, 1);

    glClearDepth(1);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    glMaterialfv(GL_FRONT, GL_SPECULAR, lightSpecular);
    glMaterialfv(GL_FRONT, GL_SHININESS, shine);
    glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_DEPTH_TEST);
    glShadeModel(GL_SMOOTH);
}

int main(int argc, char *argv[])
{
    glutInit(&argc, argv);
    glutInitWindowSize(640, 480);
    glutInitWindowPosition(10, 10);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);

    glutCreateWindow("Load OBJ");
    glutReshapeFunc(resize);
    glutDisplayFunc(display);
    glutIdleFunc(idle);

    init();
    glutMainLoop();
    return 0;
}
