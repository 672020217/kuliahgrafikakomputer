#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <stdlib.h>


static void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT);
    glBegin(GL_QUADS);
        glVertex2i(25, 55);
        glVertex2i(25, 5);
        glVertex2i(-25, 5);
        glVertex2i(-25, 55);

        glVertex2i(25, -5);
        glVertex2i(25, -55);
        glVertex2i(-25, -55);
        glVertex2i(-25, -5);
    glEnd();
    glFlush();
}


int main(int argc, char *argv[])
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DEPTH | GLUT_SINGLE | GLUT_RGBA);
    glutInitWindowPosition(100, 100);
    glutInitWindowSize(640, 640);

    glutCreateWindow("MBZ");
    gluOrtho2D(-320, 320, -320, 320);
    glutDisplayFunc(display);
    glutMainLoop();

    return EXIT_SUCCESS;
}

