#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <stdlib.h>
#include <math.h>

void belahKetupat(int posX, int posY, int horizontal, int vertical) {
    int halfHorizontal  = horizontal/2,
        halfVertical    = vertical/2;

    glBegin(GL_QUADS);
        glVertex2i(posX, posY);
        glVertex2i(posX+halfHorizontal, posY-halfVertical);
        glVertex2i(posX, posY-vertical);
        glVertex2i(posX-halfHorizontal, posY-halfVertical);
    glEnd();
}

void layangLayang(int posX, int posY, int horizontal, int upperVertical, int lowerVertical) {
    int halfHorizontal = horizontal/2;
    glBegin(GL_QUADS);
        glVertex2i(posX, posY);
        glVertex2i(posX+halfHorizontal, posY-upperVertical);
        glVertex2i(posX, posY-upperVertical-lowerVertical);
        glVertex2i(posX-halfHorizontal, posY-upperVertical);
    glEnd();
}

void lingkaran(int posX, int posY, int r) {
    int rep = 0, cr = 0;
    float cy;

    glBegin(GL_TRIANGLE_FAN);
        glVertex2f(posX, posY);

        while(true) {
            cy = sqrt(pow(r, 2)-pow(cr, 2));

            if (rep > 0 && rep < 3) cy = -cy;

            glVertex2f(posX+cr, posY+cy);

            if (rep < 2) {
                if (rep%2 == 0) cr++;
                else cr--;
            }
            else {
                if (rep%2 == 0) cr--;
                else cr++;
            }

            if (rep == 4) break;
            else if (cr == r || cr == -r || cr == 0) rep++;
        }
    glEnd();
}

static void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT);

//    Belah ketupat
    glColor3f(1, 0, 0);
    belahKetupat(-200, 230, 50, 60);
    glColor3f(0, 1, 0);
    belahKetupat(0, 235, 50, 70);
    glColor3f(0, 0, 1);
    belahKetupat(200, 240, 50, 80);

//    Layang-layang
    glColor3f(1, 0, 0);
    layangLayang(-200, 30, 50, 20, 40);
    glColor3f(0, 1, 0);
    layangLayang(0, 40, 60, 30, 50);
    glColor3f(0, 0, 1);
    layangLayang(200, 50, 70, 40, 60);

//    Lingkaran
    glColor3f(1, 0, 0);
    lingkaran(-200, -200, 50);
    glColor3f(0, 1, 0);
    lingkaran(0, -200, 75);
    glColor3f(0, 0, 1);
    lingkaran(200, -200, 100);

    glFlush();
}


int main(int argc, char *argv[])
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DEPTH | GLUT_SINGLE | GLUT_RGBA);
    glutInitWindowPosition(100, 50);
    glutInitWindowSize(640, 640);

    glutCreateWindow("MBZ");
    gluOrtho2D(-320, 320, -320, 320);
    glutDisplayFunc(display);
    glutMainLoop();

    return EXIT_SUCCESS;
}

