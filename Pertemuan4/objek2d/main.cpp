#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <stdlib.h>

void persegi(int posX, int posY, int width, int height) {
    glBegin(GL_QUADS);
        glVertex2i(posX, posY);
        glVertex2i(posX+width, posY);
        glVertex2i(posX+width, posY-height);
        glVertex2i(posX, posY-height);
    glEnd();
}

void trapesium(int posX, int posY, int atas, int bawah, int tinggi) {
    glBegin(GL_QUADS);
        glVertex2i(posX, posY);
        glVertex2i(posX+atas, posY);
        glVertex2i(posX+bawah, posY-tinggi);
        glVertex2i(posX, posY-tinggi);
    glEnd();
}

void trapesiumSamaKaki(int posX, int posY, int atas, int bawah, int tinggi) {
    int bot;
    bot = (bawah-atas)/2;
    glBegin(GL_QUADS);
        glVertex2i(posX, posY);
        glVertex2i(posX+atas, posY);
        glVertex2i(posX+atas+bot, posY-tinggi);
        glVertex2i(posX-bot, posY-tinggi);
    glEnd();
}

void jajarGenjang(int posX, int posY, int alas, int tinggi) {
    glBegin(GL_QUADS);
        glVertex2i(posX, posY);
        glVertex2i(posX+alas, posY);
        glVertex2i(posX+(alas/2), posY-tinggi);
        glVertex2i(posX-(alas/2), posY-tinggi);
    glEnd();
}

static void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT);

//    Persegi
    glColor3f(1, 0, 0);
    persegi(0, 0, 50, 50);
    glColor3f(0, 1, 0);
    persegi(-200, 50, 50, 100);
    glColor3f(0, 0, 1);
    persegi(-200, -100, 100, 50);

//    Trapesium
//    glColor3f(1, 0, 0);
//    trapesium(0, 0, 50, 75, 50);
//    glColor3f(0, 1, 0);
//    trapesium(-200, 50, 50, 100, 75);
//    glColor3f(0, 0, 1);
//    trapesium(-200, -100, 100, 50, 75);

//    Trapesium sama kaki
//    glColor3f(1, 0, 0);
//    trapesiumSamaKaki(0, 0, 50, 75, 50);
//    glColor3f(0, 1, 0);
//    trapesiumSamaKaki(-12, 100, 75, 50, 50);
//    glColor3f(0, 0, 1);
//    trapesiumSamaKaki(-200, 50, 100, 50, 60);

//    Jajar genjang
//    glColor3f(1, 0, 0);
//    jajarGenjang(0, 0, 50, 50);
//    glColor3f(0, 1, 0);
//    jajarGenjang(-200, 0, 100, 50);
//    glColor3f(0, 0, 1);
//    jajarGenjang(200, 0, 100, 100);

    glFlush();
}


int main(int argc, char *argv[])
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DEPTH | GLUT_SINGLE | GLUT_RGBA);
    glutInitWindowPosition(100, 100);
    glutInitWindowSize(640, 640);

    glutCreateWindow("MBZ");
    gluOrtho2D(-320, 320, -320, 320);
    glutDisplayFunc(display);
    glutMainLoop();

    return EXIT_SUCCESS;
}

