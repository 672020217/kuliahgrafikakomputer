#include <GL/glut.h>
#include "glm.h"

// global
    GLfloat angle = 0;
// end

void resize(int w, int h) {
    const float ar = (float) w / h;

    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);

    glLoadIdentity();
    gluPerspective(45, ar, 1, 300);
    glMatrixMode(GL_MODELVIEW);

    glLoadIdentity();
}

void display() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glPushMatrix();
        glColor3f(1, 0, 0);
        glTranslatef(-3, 0, -15);
        glRotatef((angle+=0.1), 0, 1, 0);
        glutSolidCube(3);
    glPopMatrix();

    glPushMatrix();
        glColor3f(0, 1, 0);
        glTranslatef(3, 0, -15);
        // glRotatef(60, 1, 0.3, 0);
        glRotatef((angle+=0.1), 0, 1, 0);
        // glutSolidSphere(1.5, 10, 10);
        glutSolidTeapot(2);
    glPopMatrix();

    glutSwapBuffers();
}

void idle() {
    glutPostRedisplay();
}

void key(unsigned char key, int x, int y) {
    switch (key)
    {
        // case 'y':
        //     ok+=0.1;
        //     break;
    
        default:
            break;
    }
}

void special(int key, int x, int y) {
    switch (key)
    {
        // case /* constant-expression */:
        //     /* code */
        //     break;
    
        default:
            break;
    }
}

void lightSetup() {
    GLfloat lightPos[] = {0, 0, 5, 1},
            lightDiffuse[] = {0.8, 0.8, 0.8, 1},
            lightAmbient[] = {0.2, 0.2, 0.2, 1},
            lightSepcular[] = {0.5, 0.5, 0.5, 1},
            shine[] = {80};

    glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
    glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient);
    // glLightfv(GL_LIGHT0, GL_SPECULAR, lightSepcular);
    glMaterialfv(GL_FRONT, GL_SPECULAR, lightSepcular);
    // glLightfv(GL_LIGHT0, GL_SHININESS, shine);
    glMaterialfv(GL_FRONT, GL_SHININESS, shine);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_COLOR_MATERIAL);
}

void init() {
    glutDisplayFunc(display);
    glutIdleFunc(idle);
    glutKeyboardFunc(key);
    glutSpecialFunc(special);
    glutReshapeFunc(resize);

    glClearColor(0, 0, 0, 1);
    glClearDepth(1);
    glEnable(GL_DEPTH_TEST);

    lightSetup();
}

int main(int argc, char *argv[])
{
    glutInit(&argc, argv);
    glutInitWindowSize(640, 480);
    glutInitWindowPosition(100, 100);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);

    glutCreateWindow("Light Example");

    init();
    glutMainLoop();
    return 0;
}
