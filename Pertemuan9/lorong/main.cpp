#include <GL/glut.h>
#include <math.h>
#include <time.h>
#include "Camera.h"

// Global variable
    GLfloat angleX = 90, angleY = 0;
    GLfloat ox=0, oy=0, oz=-150;
    GLfloat winWidth = 640, winHeight = 480;
    Camera camera(0, 150, 10, 2, 1);
    GLfloat lightPos[] = {0, 0, 5, 1};
// End

static void resize(int w, int h) {
    const float ar = (float) w/h;
    winWidth = w; winHeight = h;
    camera.setMaxXY(winWidth/2, winHeight/2);

    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);

    glLoadIdentity();

    gluPerspective(45, ar, 1, 300);
    glMatrixMode(GL_MODELVIEW);

    glLoadIdentity();
}

static void quads(
    GLfloat w, GLfloat h,
    GLfloat x, GLfloat y, GLfloat z, 
    GLfloat angle, GLfloat rx, GLfloat ry, GLfloat rz) {

    glPushMatrix();
        glTranslatef(x, y, z);
        glRotatef(angle, rx, ry, rz);
        glBegin(GL_QUADS);
            glNormal3f(0, 0, 1);
            glVertex3f(w/2, h/2, 0);
            glVertex3f(-w/2, h/2, 0);
            glVertex3f(-w/2, -h/2, 0);
            glVertex3f(w/2, -h/2, 0);
        glEnd();
    glPopMatrix();
}

static void drawWall(GLfloat x, GLfloat y, GLfloat z,
                     GLfloat posX, GLfloat posY, GLfloat posZ) {
    quads(x, z, posX, (y/2)+posY, posZ, 90, -1, 0, 0);
    quads(x, z, posX, (-y/2)+posY, posZ, 90, 1, 0, 0);

    quads(x, y, posX, posY, (z/2)+posZ, 0, 0, 0, 0);
    quads(x, y, posX, posY, (-z/2)+posZ, 180, -1, 0, 0);

    quads(z, y, (-x/2)+posX, posY, posZ, 90, 0, -1, 0);
    quads(z, y, (x/2)+posX, posY, posZ, 90, 0, 1, 0);
}

static void alas() {
    glColor3f(0.4745, 0.3804, 0.2392);
    quads(300, 300, 0, 0, 0, 90, -1, 0, 0);
}

static void lorong() {
    glColor3f(0.3, 0.3, 0.3);
    drawWall(1, 10, 49.5, 0, 5, -24.75);
    // glColor3f(0, 1, 0);
    drawWall(80, 10, 1, 0, 5, -50);
    // glColor3f(1, 0, 0);
    drawWall(1, 10, 39.5, 10, 5, -19.75);
    drawWall(1, 10, 39.5, -10, 5, -19.75);
    // glColor3f(0, 0, 1);
    drawWall(41, 10, 1, 30, 5, -40);
    drawWall(41, 10, 1, -30, 5, -40);
    drawWall(101, 10, 1, 0, 5, -60);
    // glColor3f(1, 0, 0);
    drawWall(1, 10, 19, 50, 5, -50);
    drawWall(1, 10, 19, -50, 5, -50);


    // glColor3f(1, 0, 0);
    drawWall(101, 1, 21, 0, 10.5, -50);
    drawWall(21, 1, 39.5, 0, 10.5, -19.75);
}

static void benda(int posX, int posY, int posZ) {
    int object = rand() % 4;
    
    glPushMatrix();
        glColor3f(rand() % 1000/1000.0, rand() % 1000/1000.0, rand() % 1000/1000.0);
        glTranslatef(posX, posY, posZ);
        glRotatef(rand() % 360, rand() % 1000/1000.0, rand() % 1000/1000.0, rand() % 1000/1000.0);

        switch (object)
        {
            case 0:
                glFrontFace(GL_CW);
                glutSolidTeapot(1);
                glFrontFace(GL_CCW);
                break;
            case 1:
                glutSolidCube(1);
                break;
            case 2:
                glutSolidTorus(0.25, 0.5, 10, 50);
                break;
            case 3:
                glutSolidTetrahedron();
                break;
        }
    glPopMatrix();
}

static void display() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // preview lorong
        // glPushMatrix();
        // glTranslatef(ox, oy, oz);
        // glRotatef(angleX, 1, 0, 0);
        // glRotatef(angleY, 0, 1, 0);
        // alas();
        // lorong();
        // glPopMatrix();
    // end

    glLoadIdentity();
    camera.setCamera();

    glPushMatrix();
        glTranslatef(0, -5, 0);
        alas();
        lorong();

        srand(time(NULL));
        for(int i = 0; i >= -45; i-=5)
        {
            if (i == 0) {
                benda(i, 1, -58);
                benda(i, 1, -52);
            }
            else {
                if (i >= -40) {
                    benda(8, 1, i);
                    benda(-8, 1, i);

                    benda(-i, 1, -48);
                    benda(i, 1, -48);

                    benda(i, 1, -52);
                    benda(-i, 1, -52);
                }
                
                if (i < -5) {
                    benda(-i, 1, -42);
                    benda(i, 1, -42);
                }
                
                benda(2, 1, i);
                benda(-2, 1, i);

                benda(i, 1, -58);
                benda(-i, 1, -58);
            }
        }
    glPopMatrix();

    glutSwapBuffers();
}

void updateLightPos() {
    lightPos[0] = camera.getEyeX();
    lightPos[2] = camera.getEyeZ();
    glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
}

static void key(unsigned char key, int x, int y) {
    // switch (key)
    // {
    //     case 'w':
    //         oy--;
    //         break;
    //     case 's':
    //         oy++;
    //         break;
    //     case 'a':
    //         ox++;
    //         break;
    //     case 'd':
    //         ox--;
    //         break;
    //     case 'i':
    //         oz++;
    //         break;
    //     case 'o':
    //         oz--;
    //         break;
    
    //     default:
    //         break;
    // }

    camera.key(key);

    if (key == 'w' || key == 'a' || key == 's' || key == 'd') {
        updateLightPos();
    }
}

static void special(int key, int x, int y) {
    switch (key)
    {
        case GLUT_KEY_UP:
            angleX++;
            break;
        case GLUT_KEY_DOWN:
            angleX--;
            break;
        case GLUT_KEY_LEFT:
            angleY++;
            break;
        case GLUT_KEY_RIGHT:
            angleY--;
            break;
    
        default:
            break;
    }
}

static void passive(int x, int y) {
    x-=winWidth/2;
    y=(winHeight/2)-y;
    camera.mouseMove(x, y);
}

static void idle() {
    glutPostRedisplay();
}

void lightSetup() {
    GLfloat lightAmbient[] = {0.2, 0.2, 0.2, 1};
    GLfloat lightDifuse[] = {0.8, 0.8, 0.8, 1};
    GLfloat lightSpecular[] = {0.5, 0.5, 0.5, 1};
    GLfloat shine[] = {80};

    glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
    glMaterialfv(GL_FRONT, GL_AMBIENT, lightAmbient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, lightDifuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, lightSpecular);
    glMaterialfv(GL_FRONT, GL_SHININESS, shine);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_COLOR_MATERIAL);

    // glFrontFace(GL_CW);
    // glCullFace(GL_FRONT);
    // glEnable(GL_CULL_FACE);
}

static void init() {
    camera.setMaxXY(winWidth/2, winHeight/2);
    glutDisplayFunc(display);
    glutReshapeFunc(resize);
    glutKeyboardFunc(key);
    glutSpecialFunc(special);
    glutPassiveMotionFunc(passive);
    glutIdleFunc(idle);

    glClearColor(0, 0, 0, 1);
    glClearDepth(1);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

    glEnable(GL_DEPTH_TEST);

    lightSetup();

    glFrontFace(GL_CCW);
    glCullFace(GL_BACK);
    glEnable(GL_CULL_FACE);
}

int main(int argc, char *argv[])
{
    glutInit(&argc, argv);
    glutInitWindowSize(winWidth, winHeight);
    glutInitWindowPosition(10, 10);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);

    glutCreateWindow("Lorong");

    init();
    glutMainLoop();
    return 0;
}
