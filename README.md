# KuliahGrafikaKomputer

Projek untuk Kuliah Grafika Komputer. Untuk vlog di youtube dapat dilihat di [playlist youtube](https://www.youtube.com/playlist?list=PLvPelp2RKEjh55hxGuAtjvedrJtjNgRRX "Playlist YouTube").

### Informasi mahasiswa

<table>
    <tr><th>Nama</th><td>Muhammad Bagus Zulmi</td></tr>
    <tr><th>NIM</th><td>170535629515</td></tr>
    <tr><th>Prodi</th><td>S1 Teknik Informatika</td></tr>
    <tr><th>Kelas</th><td>B</td></tr>
</table>

**Universitas Negeri Malang**