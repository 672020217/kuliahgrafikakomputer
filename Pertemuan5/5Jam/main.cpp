#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <ctime>

using namespace std;

void hand(int height) {
    glLineWidth(4);
    glBegin(GL_LINES);
        glVertex2i(0, 0);
        glVertex2i(0, height);
        glVertex2i(0, height);
        glVertex2i(-10, height-10);
        glVertex2i(0, height);
        glVertex2i(10, height-10);
    glEnd();
}

void numberBar(int width) {
    int quarterWidth=width/4;
    glBegin(GL_QUADS);
        glVertex2i(-quarterWidth/2, width/2);
        glVertex2i(quarterWidth/2, width/2);
        glVertex2i(quarterWidth/2, -width/2);
        glVertex2i(-quarterWidth/2, -width/2);
    glEnd();
}

void numberBarCircle(int r, int w) {
    int sudut = 0;

    for (int i=0; i<360; i+=30) {
        glPushMatrix();
            glRotatef(i, 0, 0, 1);
            glTranslatef(0, r-(w/2), 0);
            numberBar(w);
        glPopMatrix();
    }
}

void circle(int r, int posX=0, int posY=0) {
    int rep = 0, cr = 0;
    float cy;

    glBegin(GL_TRIANGLE_FAN);
        glVertex2f(posX, posY);

        while(true) {
            cy = sqrt(pow(r, 2)-pow(cr, 2));

            if (rep > 0 && rep < 3) cy = -cy;

            glVertex2f(posX+cr, posY+cy);

            if (rep < 2) {
                if (rep%2 == 0) cr++;
                else cr--;
            }
            else {
                if (rep%2 == 0) cr--;
                else cr++;
            }

            if (rep == 4) break;
            else if (cr == r || cr == -r || cr == 0) rep++;
        }
    glEnd();
}

GLfloat s=0, m=0, h=0;

static void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT);

    /// hour circle
    glColor3f(0.88627,0.88627, 0.88627);
    circle(230);

    glColor3f(0, 0, 0);
    numberBarCircle(230, 30);

    /// minute circle
    glColor3f(0.16863, 0.17647, 0.21176);
    circle(90, -80, 80);

    glColor3f(0.77778, 0.77778, 0.77778);
    glPushMatrix();
        glTranslatef(-80, 80, 0);
        numberBarCircle(90, 20);
    glPopMatrix();

    /// second circle
    glColor3f(0.16863, 0.17647, 0.21176);
    circle(60, 90, -90);

    glColor3f(0.77778, 0.77778, 0.77778);
    glPushMatrix();
        glTranslatef(90, -90, 0);
        numberBarCircle(60, 10);
    glPopMatrix();

    /// second
    glColor3f(0.77778, 0.77778, 0.77778);
    circle(5, 90, -90);
    glPushMatrix();
        glTranslated(90, -90, 0);
        glRotatef(s, 0, 0, -1);
        hand(40);
    glPopMatrix();

    /// minute
    glColor3f(0.77778, 0.77778, 0.77778);
    circle(7, -80, 80);

    glPushMatrix();
        glTranslatef(-80, 80, 0);
        glRotatef(m, 0, 0, -1);
        hand(70);
    glPopMatrix();

    /// hour
    glColor3f(0.73725, 0.17254, 0.17254);
    circle(10);

    glPushMatrix();
        glRotatef(h, 0, 0, -1);
        hand(200);
    glPopMatrix();

    s+=6;
    m+=0.1;
    h+=(30./3600);

    if (s==360) s=0;
    if (abs(m-360) < 0.1) m=0;
    if (abs(h-360) < 0.001) h=0;

    glFlush();
}

void timer(int v) {
    glutPostRedisplay();
    glutTimerFunc(1000, timer, 0);
}

void initTime() {
    time_t t= time(0);
    tm* now = localtime(&t);

    float currentH = now->tm_hour;
    float currentM = now->tm_min;
    int currentS = now->tm_sec;
    currentM += (float)currentS/60;
    currentH += currentM/60;

    s = currentS*6;
    m = currentM*6;
    h = currentH*30;
}

int main(int argc, char *argv[])
{
    initTime();
    glutInit(&argc, argv);
    glutInitWindowSize(640,640);
    glutInitWindowPosition(10,10);
    glutInitDisplayMode(GLUT_RGBA | GLUT_SINGLE | GLUT_DEPTH);

    glutCreateWindow("Jam");
    glClearColor(0.16863, 0.17647, 0.21176, 0);
    gluOrtho2D(-320, 320, -320, 320);
    glutTimerFunc(1000, timer, 0);
    glutDisplayFunc(display);

    glutMainLoop();

    return EXIT_SUCCESS;
}
