#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <stdlib.h>

void drawBaling(int posX, int posY, int width, int height) {
    glBegin(GL_TRIANGLES);
        glVertex2i(posX, posY);
        glVertex2i(posX-(width/2), posY+(height/2));
        glVertex2i(posX+(width/2), posY+(height/2));
    glEnd();

    glBegin(GL_TRIANGLES);
        glVertex2i(posX, posY);
        glVertex2i(posX-(width/2), posY-(height/2));
        glVertex2i(posX+(width/2), posY-(height/2));
    glEnd();
}

GLfloat sudut = 1;

static void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT);

    glPushMatrix();
    glColor3f(0.73725, 0.17254, 0.17254);
        glRotatef(sudut, 0, 0, 1);
        drawBaling(0, 0, 100, 200);
    glPopMatrix();

    glPushMatrix();
        glRotatef(sudut, 0, 0, -1);
        glColor3f(0.88627,0.88627, 0.88627);
        drawBaling(0, 0, 100, 200);
    glPopMatrix();

    sudut++;
    if (sudut == 360) sudut = 0;

    glFlush();
}

void timer(int v) {
    glutPostRedisplay();
    glutTimerFunc(1000/100, timer, 0);
}

int main(int argc, char *argv[])
{
    glutInit(&argc, argv);
    glutInitWindowSize(640,640);
    glutInitWindowPosition(10,10);
    glutInitDisplayMode(GLUT_RGBA | GLUT_SINGLE | GLUT_DEPTH);

    glutCreateWindow("Baling - baling");
    glClearColor(0.16863, 0.17647, 0.21176, 0);
    gluOrtho2D(-320, 320, -320, 320);
    glutTimerFunc(1000/100, timer, 0);
    glutDisplayFunc(display);

    glutMainLoop();

    return EXIT_SUCCESS;
}
