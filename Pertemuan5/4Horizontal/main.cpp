#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <stdlib.h>

int posX = 0, posY = 0, xSpeed = 10, xMin = -320, xMax = 320;

void persegi(int posX, int posY, int width, int height) {
    glBegin(GL_QUADS);
        glVertex2i(posX, posY);
        glVertex2i(posX+width, posY);
        glVertex2i(posX+width, posY-height);
        glVertex2i(posX, posY-height);
    glEnd();
}

static void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT);
    persegi(posX, posY, 100, 100);
    posX += xSpeed;

    if (posX >= xMax-100 || posX <= xMin) xSpeed = -xSpeed;
    glFlush();
}

void timer(int) {
    glutPostRedisplay();
    glutTimerFunc(1000/30, timer, 0);
}

int main(int argc, char *argv[])
{
    glutInit(&argc, argv);
    glutInitWindowSize(640,640);
    glutInitWindowPosition(10,10);
    glutInitDisplayMode(GLUT_RGBA | GLUT_SINGLE | GLUT_DEPTH);

    glutCreateWindow("Horizontal");
    glutDisplayFunc(display);
    glutTimerFunc(1000/30, timer, 0);
    gluOrtho2D(-320, 320, -320, 320);
    glutMainLoop();

    return EXIT_SUCCESS;
}
