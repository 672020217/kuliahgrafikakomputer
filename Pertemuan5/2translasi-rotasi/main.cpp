#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <stdlib.h>

void persegi(int posX, int posY, int width, int height) {
    glBegin(GL_QUADS);
        glVertex2i(posX, posY);
        glVertex2i(posX+width, posY);
        glVertex2i(posX+width, posY-height);
        glVertex2i(posX, posY-height);
    glEnd();
}

static void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT);

    glColor3f(1, 1, 1);
    glPushMatrix();
        glTranslatef(100, 50, 0);
        glRotated(30, 0, 0, 1);
        persegi(0, 0, 100, 100);
    glPopMatrix();

    glColor3f(1, 0, 0);
    glPushMatrix();
        glRotated(30, 0, 0, 1);
        glTranslatef(100, 50, 0);
        persegi(0, 0, 100, 100);
    glPopMatrix();

    glFlush();
}

int main(int argc, char *argv[])
{
    glutInit(&argc, argv);
    glutInitWindowSize(640,640);
    glutInitWindowPosition(10,10);
    glutInitDisplayMode(GLUT_RGBA | GLUT_SINGLE| GLUT_DEPTH);

    glutCreateWindow("Translasi Rotasi");
    gluOrtho2D(-320, 320, -320, 320);
    glutDisplayFunc(display);

    glutMainLoop();

    return EXIT_SUCCESS;
}
