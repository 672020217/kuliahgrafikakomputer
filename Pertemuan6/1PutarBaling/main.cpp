#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <stdlib.h>

/// Global variables
bool putar = false;
GLdouble speed = 2, direction = -1;

void drawBaling(int posX, int posY, int width, int height) {
    glBegin(GL_TRIANGLES);
        glVertex2i(posX, posY);
        glVertex2i(posX-(width/2), posY+(height/2));
        glVertex2i(posX+(width/2), posY+(height/2));

        glVertex2i(posX, posY);
        glVertex2i(posX-(width/2), posY-(height/2));
        glVertex2i(posX+(width/2), posY-(height/2));
    glEnd();
}

static void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT);
    if (putar) glRotatef(speed, 0, 0, direction);
    drawBaling(0, 0, 100, 200);
    glFlush();
}

static void key(unsigned char key, int x, int y)
{
    switch (key) {
    case 'k':
        putar = true;
        direction = -1;
        break;
    case 'l':
        putar = true;
        direction = 1;
        break;
    case ' ':
        putar = false;
        break;
    }
}

static void idle(void)
{
    glutPostRedisplay();
}

static void timer(int) {
    glutPostRedisplay();
    glutTimerFunc(1000/60, timer, 0);
}

int main(int argc, char *argv[])
{
    glutInit(&argc, argv);
    glutInitWindowSize(640,640);
    glutInitWindowPosition(10,10);
    glutInitDisplayMode(GLUT_RGBA | GLUT_SINGLE| GLUT_DEPTH);

    glutCreateWindow("Baling Baling");
    glClearColor(0.2222, 0.2222, 0.2222, 0);
    gluOrtho2D(-320, 320, -320, 320);
    glutDisplayFunc(display);
    glutKeyboardFunc(key);
//    glutIdleFunc(idle);
    glutTimerFunc(1000/60, timer, 0);

    glutMainLoop();

    return EXIT_SUCCESS;
}
