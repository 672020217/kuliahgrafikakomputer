#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <stdlib.h>

/// Global variables
int kx = 0, ky = 0; /// kotak
int sx = 0, sy = 0; /// segitiga
int speed = 10;

void kotak(int h) {
    glBegin(GL_QUADS);
        glVertex2d(0, h/2);
        glVertex2d(h/2, 0);
        glVertex2d(0, -h/2);
        glVertex2d(-h/2, 0);
    glEnd();
}

void segitiga(int a, int t) {
    glBegin(GL_TRIANGLES);
        glVertex2d(0, t/2);
        glVertex2d(a/2, -t/2);
        glVertex2d(-a/2, -t/2);
    glEnd();
}

static void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT);

    glPushMatrix();
        glTranslatef(kx, ky, 0);
        glColor3f(1, 1, 1);
        kotak(50);
    glPopMatrix();

    glPushMatrix();
        glTranslatef(sx, sy, 0);
        glColor3f(1, 0, 0);
        segitiga(50, 50);
    glPopMatrix();

    glFlush();
}

static void key(unsigned char key, int x, int y)
{
    switch (key) {
    case 'w':
        ky+=speed;
        break;
    case 'a':
        kx-=speed;
        break;
    case 's':
        ky-=speed;
        break;
    case 'd':
        kx+=speed;
        break;
    }
}

static void specialKey(int key, int x, int y) {
    switch (key) {
    case GLUT_KEY_UP:
        sy+=speed;
        break;
    case GLUT_KEY_LEFT:
        sx-=speed;
        break;
    case GLUT_KEY_DOWN:
        sy-=speed;
        break;
    case GLUT_KEY_RIGHT:
        sx+=speed;
        break;
    }
}

static void idle(void)
{
    glutPostRedisplay();
}

int main(int argc, char *argv[])
{
    glutInit(&argc, argv);
    glutInitWindowSize(640,480);
    glutInitWindowPosition(10,10);
    glutInitDisplayMode(GLUT_RGBA | GLUT_SINGLE | GLUT_DEPTH);

    glutCreateWindow("Kotak Segitiga");

    gluOrtho2D(-320, 320, -240, 240);
    glutDisplayFunc(display);
    glutKeyboardFunc(key);
    glutSpecialFunc(specialKey);
    glutIdleFunc(idle);

    glutMainLoop();

    return EXIT_SUCCESS;
}
