#include <GL/glut.h>
#include <math.h>
#include "Imageloader.h"

#define _USE_MATH_DEFINES

// global
    GLuint texId;
    GLfloat angleX = 0, angleY = 0;
// end

struct coor {
    GLfloat pos[2];
};

void resize(int w, int h) {
    const float ar = (float) w/h;

    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);

    glLoadIdentity();
    gluPerspective(45, ar, 1, 100);
    glMatrixMode(GL_MODELVIEW);

    glLoadIdentity();
}

GLfloat degToRad(GLfloat deg) {
    return deg*M_PI/180;
}

void drawSegitiga(GLfloat a, GLfloat t, GLfloat h, GLfloat vFlip, GLfloat x, GLfloat z, coor c[]) {
    glPushMatrix();
        glTranslatef(x, -h/2, z);
        glRotatef(180*vFlip, 0, 1, 0);
        glNormal3f(0, 0, 1);
        glRotatef(30, -1, 0, 0);
        glBegin(GL_TRIANGLES);
            glTexCoord2fv(c[0].pos);
            glVertex3f(-a/2, 0, 0);
            glTexCoord2fv(c[1].pos);
            glVertex3f(a/2, 0, 0);
            glTexCoord2fv(c[2].pos);
            glVertex3f(0, t, 0);
        glEnd();
    glPopMatrix();
}

void drawLimas(GLfloat size) {
    GLfloat h = sin(degToRad(60))*size;
    coor c[4];

    //coor
        c[0].pos[0] = 0.33333;
        c[0].pos[1] = 0.33333;
        c[1].pos[0] = 0.66667;
        c[1].pos[1] = 0.33333;
        c[2].pos[0] = 0.66667;
        c[2].pos[1] = 0.66667;
        c[3].pos[0] = 0.33333;
        c[3].pos[1] = 0.66667;
    //end
    glNormal3f(0, -1, 0);
    glBegin(GL_QUADS);
        glTexCoord2fv(c[0].pos);
        glVertex3f(-size/2, -h/2, -size/2);
        glTexCoord2fv(c[1].pos);
        glVertex3f(size/2, -h/2, -size/2);
        glTexCoord2fv(c[2].pos);
        glVertex3f(size/2, -h/2, size/2);
        glTexCoord2fv(c[3].pos);
        glVertex3f(-size/2, -h/2, size/2);
    glEnd();

    //depan
    // coor
        c[0].pos[0] = 0.33333;
        c[0].pos[1] = 0.66667;
        c[1].pos[0] = 0.66667;
        c[1].pos[1] = 0.66667;
        c[2].pos[0] = 0.5;
        c[2].pos[1] = 1;
    //end
    drawSegitiga(size, size, h, 0, 0, size/2, c);
    //kiri
    //coor
        c[0].pos[0] = 0.33333;
        c[0].pos[1] = 0.33333;
        c[1].pos[0] = 0.33333;
        c[1].pos[1] = 0.66667;
        c[2].pos[0] = 0;
        c[2].pos[1] = 0.5;
    //end
    drawSegitiga(size, size, h, -0.5, -size/2, 0, c);
    //belakang
    //coor
        c[0].pos[0] = 0.66667;
        c[0].pos[1] = 0.33333;
        c[1].pos[0] = 0.33333;
        c[1].pos[1] = 0.33333;
        c[2].pos[0] = 0.5;
        c[2].pos[1] = 0;
    //end
    drawSegitiga(size, size, h, 1, 0, -size/2, c);
    //kanan
    //coor
        c[0].pos[0] = 0.66667;
        c[0].pos[1] = 0.66667;
        c[1].pos[0] = 0.66667;
        c[1].pos[1] = 0.33333;
        c[2].pos[0] = 1;
        c[2].pos[1] = 0.5;
    //end
    drawSegitiga(size, size, h, 0.5, size/2, 0, c);
}

void display() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texId);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    glLoadIdentity();
    glTranslatef(0, 0, -10);
    glRotatef(angleX, 1, 0, 0);
    glRotatef(angleY, 0, 1, 0);

    drawLimas(3);

    glDisable(GL_TEXTURE);
    
    glutSwapBuffers();
}

void special(int key, int x, int y) {
    switch (key)
    {
        case GLUT_KEY_UP:
            angleX++;
            break;
        case GLUT_KEY_DOWN:
            angleX--;
            break;
        case GLUT_KEY_LEFT:
            angleY++;
            break;
        case GLUT_KEY_RIGHT:
            angleY--;
            break;
    
        default:
            break;
    }
}

void idle() {
    glutPostRedisplay();
}

void getImage() {
    Image *img = loadBMP("/home/mbaguszulmi/Documents/SMT4/Grafika-Komputer/kuliahgrafikakomputer/Pertemuan10/limas/texturelimas.bmp");
    loadTextures(img, &texId, 1);
}

void lightSetup() {
    GLfloat lightPos[] = {0, 0, 5, 1},
            lightDiffuse[] = {0.8, 0.8, 0.8, 1},
            lightAmbient[] = {0.2, 0.2, 0.2, 1},
            lightSpecular[] = {0.5, 0.5, 0.5, 1},
            shine[] = {80};

    glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
    glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient);

    glMaterialfv(GL_FRONT, GL_SPECULAR, lightSpecular);
    glMaterialfv(GL_FRONT, GL_SHININESS, shine);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_COLOR_MATERIAL);
}

void init() {
    glutDisplayFunc(display);
    glutReshapeFunc(resize);
    glutSpecialFunc(special);
    glutIdleFunc(idle);

    glClearColor(1, 1, 1, 1);
    glClearDepth(1);
    glEnable(GL_DEPTH_TEST);

    lightSetup();

    getImage();
}

int main(int argc, char *argv[])
{
    glutInit(&argc, argv);
    glutInitWindowSize(640, 480);
    glutInitWindowPosition(100, 100);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
    glutCreateWindow("Limas Texture");

    init();

    glutMainLoop();
    return 0;
}
